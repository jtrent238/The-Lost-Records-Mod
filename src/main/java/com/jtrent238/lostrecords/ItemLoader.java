package com.jtrent238.lostrecords;

import com.jtrent238.lostrecords.items.ItemLostRecord;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemLoader {

	public static Item ItemRecord0;
	public static Item ItemRecord1;
	public static Item ItemRecord2;
	public static Item ItemRecord3;
	public static Item ItemRecord4;
	public static Item ItemRecord5;
	public static Item ItemRecord6;
	public static Item ItemRecord7;
	public static Item ItemRecord8;
	public static Item ItemRecord9;
	public static Item ItemRecord10;
	public static Item ItemRecord11;
	public static Item ItemRecord12;
	public static Item ItemRecord13;
	public static Item ItemRecord14;
	public static Item ItemRecord15;
	public static Item ItemRecord16;
	public static Item ItemRecord17;
	public static Item ItemRecord18;
	public static Item ItemRecord19;
	public static Item ItemRecord20;
	public static Item ItemRecord21;
	public static Item ItemRecord22;
	public static Item ItemRecord23;
	public static Item ItemRecord24;
	public static Item ItemRecord25;
	public static Item ItemRecord26;
	public static Item ItemRecord27;
	public static Item ItemRecord28;
	public static Item ItemRecord29;
	public static Item ItemRecord30;
	public static Item ItemRecord31;
	public static Item ItemRecord32;
	public static Item ItemRecord33;
	public static Item ItemRecord34;
	public static Item ItemRecord35;
	public static Item ItemRecord36;
	public static Item ItemRecord37;
	public static Item ItemRecord38;
	public static Item ItemRecord39;
	public static Item ItemRecord40;
	
	public static void LoadItems() {

		ItemRecord0  = new ItemLostRecord("key").setUnlocalizedName("ItemRecord0").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord1  = new ItemLostRecord("door").setUnlocalizedName("ItemRecord1").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord2  = new ItemLostRecord("sl").setUnlocalizedName("ItemRecord2").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord3  = new ItemLostRecord("death").setUnlocalizedName("ItemRecord3").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord4  = new ItemLostRecord("lm").setUnlocalizedName("ItemRecord4").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord5  = new ItemLostRecord("mc").setUnlocalizedName("ItemRecord5").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord6 = new ItemLostRecord("hs").setUnlocalizedName("ItemRecord6").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord7  = new ItemLostRecord("minecraft").setUnlocalizedName("ItemRecord7").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord8  = new ItemLostRecord("oxygene").setUnlocalizedName("ItemRecord8").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord9  = new ItemLostRecord("equinoxe").setUnlocalizedName("ItemRecord9").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord10  = new ItemLostRecord("mov").setUnlocalizedName("ItemRecord10").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord11  = new ItemLostRecord("dh").setUnlocalizedName("ItemRecord11").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord12  = new ItemLostRecord("wh").setUnlocalizedName("ItemRecord12").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord13  = new ItemLostRecord("clark").setUnlocalizedName("ItemRecord13").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord14  = new ItemLostRecord("chris").setUnlocalizedName("ItemRecord14").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord15  = new ItemLostRecord("thirteen").setUnlocalizedName("ItemRecord15").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord16  = new ItemLostRecord("excuse").setUnlocalizedName("ItemRecord16").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord17  = new ItemLostRecord("sweden").setUnlocalizedName("ItemRecord17").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord18  = new ItemLostRecord("cat").setUnlocalizedName("ItemRecord18").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord19  = new ItemLostRecord("chirp").setUnlocalizedName("ItemRecord19").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord20  = new ItemLostRecord("danny").setUnlocalizedName("ItemRecord20").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord21  = new ItemLostRecord("beginning").setUnlocalizedName("ItemRecord21").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord22  = new ItemLostRecord("dlr").setUnlocalizedName("ItemRecord22").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord23  = new ItemLostRecord("dlyf").setUnlocalizedName("ItemRecord23").setCreativeTab(CreativeTabs.tabMisc);
		/*ItemRecord24  = new ItemLostRecord("").setUnlocalizedName("ItemRecord24").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord25  = new ItemLostRecord("").setUnlocalizedName("ItemRecord25").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord26  = new ItemLostRecord("").setUnlocalizedName("ItemRecord26").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord27  = new ItemLostRecord("").setUnlocalizedName("ItemRecord27").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord28  = new ItemLostRecord("").setUnlocalizedName("ItemRecord28").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord29  = new ItemLostRecord("").setUnlocalizedName("ItemRecord29").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord30  = new ItemLostRecord("").setUnlocalizedName("ItemRecord30").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord31  = new ItemLostRecord("").setUnlocalizedName("ItemRecord31").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord32  = new ItemLostRecord("").setUnlocalizedName("ItemRecord32").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord33  = new ItemLostRecord("").setUnlocalizedName("ItemRecord33").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord34  = new ItemLostRecord("").setUnlocalizedName("ItemRecord34").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord35  = new ItemLostRecord("").setUnlocalizedName("ItemRecord35").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord36  = new ItemLostRecord("").setUnlocalizedName("ItemRecord36").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord37  = new ItemLostRecord("").setUnlocalizedName("ItemRecord37").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord38  = new ItemLostRecord("").setUnlocalizedName("ItemRecord38").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord39  = new ItemLostRecord("").setUnlocalizedName("ItemRecord39").setCreativeTab(CreativeTabs.tabMisc);
		ItemRecord40  = new ItemLostRecord("").setUnlocalizedName("ItemRecord40").setCreativeTab(CreativeTabs.tabMisc);
		*/
		registerItems();
	}

	private static void registerItems() {
		GameRegistry.registerItem(ItemRecord0, ItemRecord0.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord1, ItemRecord1.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord2, ItemRecord2.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord3, ItemRecord3.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord4, ItemRecord4.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord5, ItemRecord5.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord6, ItemRecord6.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord7, ItemRecord7.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord8, ItemRecord8.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord9, ItemRecord9.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord10, ItemRecord10.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord11, ItemRecord11.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord12, ItemRecord12.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord13, ItemRecord13.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord14, ItemRecord14.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord15, ItemRecord15.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord16, ItemRecord16.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord17, ItemRecord17.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord18, ItemRecord18.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord19, ItemRecord19.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord20, ItemRecord20.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord21, ItemRecord21.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord22, ItemRecord22.getUnlocalizedName().substring(5));
		GameRegistry.registerItem(ItemRecord23, ItemRecord23.getUnlocalizedName().substring(5));
		
	}

}
